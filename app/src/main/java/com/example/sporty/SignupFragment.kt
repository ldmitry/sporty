package com.example.sporty

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.telephony.PhoneNumberFormattingTextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_sign_up.*


class SignupFragment: Fragment(R.layout.fragment_sign_up) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        root.setOnClickListener {
            hideKeyboard(requireActivity())
            phone_text.clearFocus()
        }
        phone_text.addTextChangedListener(PhoneNumberFormattingTextWatcher())

        phone_continue.setOnClickListener {
            val alert = AlertDialog.Builder(requireContext())
            val edittext = EditText(requireContext())
            edittext.hint = "Код из сообщения"
            alert.setTitle("Введите код из SMS-сообщения")

            alert.setView(edittext)

            alert.setPositiveButton("Подтвердить") { _, _ ->

            }
            alert.show()
        }
        super.onViewCreated(view, savedInstanceState)
    }

    private fun hideKeyboard(activity: Activity) {
        val view =
            activity.findViewById<View>(android.R.id.content)
        if (view != null) {
            val imm =
                activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}