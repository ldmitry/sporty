package com.example.sporty.my_profile
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.sporty.R
import com.example.sporty.my_profile.Podpiski
import kotlinx.android.synthetic.main.profile_bar.*

class MyProfileFragment: Fragment(R.layout.my_profile_fragment) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscriptions_button.setOnClickListener{
            val fragment = Podpiski( )
            fragment.show(requireFragmentManager(), "TAG")
        }
    }

}