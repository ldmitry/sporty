package com.example.sporty

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.sporty.routerActivity.IMapDelegate
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions


class MapsFragment : Fragment() {

    private val callback = OnMapReadyCallback { googleMap ->
        /**
         * Manipulates the map once available.
         * This callback is triggered when the map is ready to be used.
         * This is where we can add markers or lines, add listeners or move the camera.
         * In this case, we just add a marker near Sydney, Australia.
         * If Google Play services is not installed on the device, the user will be prompted to
         * install it inside the SupportMapFragment. This method will only be triggered once the
         * user has installed Google Play services and returned to the app.
         */
        val spartakNovisibirsk = LatLng(55.038341319870995, 82.92398339157653)

        //todo поставить правильные широту долготу
        val flamingoPosition = LatLng(54.93274293891338, 82.91352112868123)
        val naberezhnaya = LatLng(55.010413633675746, 82.92930372670497)
        val dvor = LatLng(54.98889844927875, 82.90150893228264)

        if (ContextCompat.checkSelfPermission(requireContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) ==
            PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission(requireContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
            PackageManager.PERMISSION_GRANTED) {
            googleMap.isMyLocationEnabled = true
            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        } else {
            ActivityCompat.requestPermissions(
                requireActivity(), arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ), MY_PERMISSIONS_REQUEST_LOCATION)
        }
        googleMap.addMarker(MarkerOptions().position(spartakNovisibirsk).title("Stadium Spartek").snippet("New info"))
        googleMap.addMarker(MarkerOptions().position(flamingoPosition).title("Stadium Flamingo").snippet("New info"))
        googleMap.addMarker(MarkerOptions().position(naberezhnaya).title("Mihailovskaya Nabereznaya").snippet("New info"))
        googleMap.addMarker(MarkerOptions().position(dvor).title("dvo").snippet("New info"))
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(spartakNovisibirsk))
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(12F), 2000, null);
        googleMap.setOnMarkerClickListener {
            (requireActivity() as IMapDelegate).onMarkerClicked()
            return@setOnMarkerClickListener true
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }

    companion object {
        private const val MY_PERMISSIONS_REQUEST_LOCATION = 99
    }
}