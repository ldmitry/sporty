package com.example.sporty.sportSelector

import com.example.sporty.R

enum class SportType(val drawable: Int, val nameText: String) {
    BADMINTON(R.drawable.ic_badminton, "Бадминтон"),
    BASKETBALL(R.drawable.ic_basketball, "Баскетбол"),
    BOX(R.drawable.ic_box, "Бокс"),
    BIG_TENNIS(R.drawable.ic_tennis, "Большой теннис"),
    GOLF(R.drawable.ic_golf, "Гольф"),
    FOOTBALL(R.drawable.ic_football, "Футбол")
}