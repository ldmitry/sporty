package com.example.sporty.sportSelector

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.sporty.R
import kotlinx.android.synthetic.main.select_sport_fragment.*

class SportSelectorFragment: Fragment(R.layout.select_sport_fragment) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sport_recycler.adapter = SportSelectorAdapter()

        sportsman_container.setOnClickListener {
            sportsman_container.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.light_blue))
            sportsman_title.setTextColor(Color.WHITE)

            trener_container.setBackgroundColor(Color.TRANSPARENT)
            trener_title.setTextColor(ContextCompat.getColor(requireContext(), R.color.light_blue))
        }

        trener_container.setOnClickListener {
            trener_container.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.light_blue))
            trener_title.setTextColor(Color.WHITE)

            sportsman_container.setBackgroundColor(Color.TRANSPARENT)
            sportsman_title.setTextColor(ContextCompat.getColor(requireContext(), R.color.light_blue))
        }
    }
}