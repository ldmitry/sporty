package com.example.sporty.sportSelector

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.sporty.R

class SportSelectorAdapter : RecyclerView.Adapter<SportSelectorAdapter.SportViewHolder>() {

    val items = SportType.values().toList()

    val selectedItems = mutableListOf<SportType>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SportViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return SportViewHolder(inflater.inflate(R.layout.sport_item, parent, false))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: SportViewHolder, position: Int) {
        val item = items[position]
        holder.setName(item.nameText)
        holder.setPreview(item.drawable)
        holder.setSelected(selectedItems.contains(items[position]))
    }

    inner class SportViewHolder(private val v: View) : RecyclerView.ViewHolder(v) {

        private val preview: ImageView = v.findViewById(R.id.sport_preview)
        private val nameText: TextView = v.findViewById(R.id.sport_name)
        private val root: ConstraintLayout = v.findViewById(R.id.root)

        init {
            root.setOnClickListener {
                if (selectedItems.contains(items[absoluteAdapterPosition])) {
                    selectedItems.remove(items[absoluteAdapterPosition])
                } else {
                    selectedItems.add(items[absoluteAdapterPosition])
                }
                notifyItemChanged(absoluteAdapterPosition)
            }
        }

        fun setSelected(isSelected: Boolean) {
            if (isSelected) {
                nameText.setTextColor(Color.WHITE)
                root.setBackgroundColor(ContextCompat.getColor(v.context, R.color.light_blue))
            } else {
                nameText.setTextColor(ContextCompat.getColor(v.context, R.color.light_blue))
                root.setBackgroundColor(Color.TRANSPARENT)
            }
        }

        fun setPreview(res: Int) {
            preview.setImageResource(res)
        }

        fun setName(name: String) {
            nameText.text = name
        }
    }
}