package com.example.sporty.myevents

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.sporty.R
import com.example.sporty.event.AppliedEvents
import com.example.sporty.event.Event

class MyEventAdapter(
    private val listener: Listener
) : RecyclerView.Adapter<MyEventAdapter.MyEventViewHolder>() {

    enum class State {
        APPLIED_BY_ME,
        CREATED_BY_ME,
        VISITED
    }

    var state = State.APPLIED_BY_ME

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyEventViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return when(state) {
            State.APPLIED_BY_ME -> MyEventViewHolder(inflater.inflate(R.layout.item_my_event, parent, false))
            State.CREATED_BY_ME -> MyEventViewHolder(inflater.inflate(R.layout.item_my_event, parent, false))
            State.VISITED -> MyEventViewHolder(inflater.inflate(R.layout.item_my_visited_event, parent, false))
        }
    }

    override fun getItemCount(): Int {

        when(state) {
            State.APPLIED_BY_ME -> AppliedEvents.getEvents().size
            State.CREATED_BY_ME -> AppliedEvents.getCreatedByMeEvents().size
            State.VISITED -> AppliedEvents.getVisitedEvents().size
        }
        return if (state == State.APPLIED_BY_ME) {
            AppliedEvents.getEvents().size
        } else {
            AppliedEvents.getCreatedByMeEvents().size
        }
    }

    fun switchToMyEvents() {
        state = State.CREATED_BY_ME
        notifyDataSetChanged()
    }

    fun switchToAppliedEvents() {
        state = State.APPLIED_BY_ME
        notifyDataSetChanged()
    }

    fun switchToVisitedEvents() {
        state = State.VISITED
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: MyEventViewHolder, position: Int) {

        when(state) {
            State.APPLIED_BY_ME -> holder.setData(AppliedEvents.getEvents()[position])
            State.CREATED_BY_ME -> holder.setData(AppliedEvents.getCreatedByMeEvents()[position])
            State.VISITED -> holder.setData(AppliedEvents.getVisitedEvents()[position])
        }
    }

    inner class MyEventViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val sportType: TextView = itemView.findViewById(R.id.sport_type_value)
        val place: TextView = itemView.findViewById(R.id.place_value)
        val time: TextView = itemView.findViewById(R.id.time_value)
        val freeRecords: TextView = itemView.findViewById(R.id.records_value)
        val allRecords: TextView = itemView.findViewById(R.id.records_value_count)
//        val additionButton: Button = itemView.findViewById(R.id.additional_info)
//        val cancelButton: Button = itemView.findViewById(R.id.cancel_button)
//        val chatButton: Button = itemView.findViewById(R.id.chat)

        init {

        }

        fun setData(event: Event) {
            sportType.text = event.sportName
            place.text = event.place
            time.text = event.date
            freeRecords.text = (event.freePlace - 1 ).toString()
            allRecords.text = event.allPlaceCount.toString()
//            if (!event.isWhatsAppEnabled) chatButton.visibility = View.GONE else chatButton.visibility = View.VISIBLE
        }
    }

    interface Listener {
        fun onAdditionalInfoClicked(index: Int)

        fun onCancelClicked(index: Int)

        fun onChatClicked()
    }
}