package com.example.sporty.myevents

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.sporty.R
import kotlinx.android.synthetic.main.my_events_fragment.*

class MyEventFragment: Fragment(R.layout.my_events_fragment) {

    val listenerImpl = object: MyEventAdapter.Listener {
        override fun onAdditionalInfoClicked(index: Int) {
            TODO("Not yet implemented")
        }

        override fun onCancelClicked(index: Int) {
            TODO("Not yet implemented")
        }

        override fun onChatClicked() {
            TODO("Not yet implemented")
        }

    }

    private lateinit var adapter: MyEventAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = MyEventAdapter(listenerImpl)
        event_reycler.adapter = adapter

        record_events.setOnClickListener {
            adapter.switchToAppliedEvents()
            record_events.setBackgroundResource(android.R.color.holo_blue_dark)
            record_events.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))

            my_events.setBackgroundResource(R.color.white)
            my_events.setTextColor(ContextCompat.getColor(requireContext(), android.R.color.holo_blue_dark))

            was_events.setBackgroundResource(R.color.white)
            was_events.setTextColor(ContextCompat.getColor(requireContext(), android.R.color.holo_blue_dark))

        }

        my_events.setOnClickListener {
            adapter.switchToMyEvents()
            record_events.setBackgroundResource(R.color.white)
            record_events.setTextColor(ContextCompat.getColor(requireContext(), android.R.color.holo_blue_dark))

            my_events.setBackgroundResource(android.R.color.holo_blue_dark)
            my_events.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))

            was_events.setBackgroundResource(R.color.white)
            was_events.setTextColor(ContextCompat.getColor(requireContext(), android.R.color.holo_blue_dark))
        }

        was_events.setOnClickListener {
            adapter.switchToVisitedEvents()
            my_events.setBackgroundResource(R.color.white)
            my_events.setTextColor(ContextCompat.getColor(requireContext(), android.R.color.holo_blue_dark))

            was_events.setBackgroundResource(android.R.color.holo_blue_dark)
            was_events.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))

            record_events.setBackgroundResource(R.color.white)
            record_events.setTextColor(ContextCompat.getColor(requireContext(), android.R.color.holo_blue_dark))
        }
    }
}