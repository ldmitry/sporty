package com.example.sporty

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.sporty.news.NewsFragment
import com.example.sporty.routerActivity.RoutingActivity
import com.example.sporty.sportSelector.SportSelectorFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        supportFragmentManager.beginTransaction().replace(R.id.root,
//            NewsFragment()
//        ).commitAllowingStateLoss()

        val intent = Intent(this, RoutingActivity::class.java)
        startActivity(intent)
//        supportFragmentManager.beginTransaction().replace(R.id.root, SportSelectorFragment()).commitAllowingStateLoss()
    }
}