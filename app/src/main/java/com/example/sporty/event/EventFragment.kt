package com.example.sporty.event

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.sporty.R
import kotlinx.android.synthetic.main.event_fragment.*

class EventFragment: Fragment(R.layout.event_fragment) {

    private val model = EventModel()

    private val listenerImpl = object : EventAdapter.Listener {
        override fun onAdditionalInfoClicked(index: Int) {
            val fullInfoEventFragment = FullEventFragment()

            fullInfoEventFragment.setEvent(model.events[index])

            fullInfoEventFragment.show(requireFragmentManager(), "BLABLA")
        }

        override fun onRegisterClicked(index: Int) {
            AppliedEvents.applyEvent(model.events[index])

            val alert = AlertDialog.Builder(requireContext())

            alert.setTitle("Вы записались на мероприятие")
            alert.setPositiveButton("Ок") { dialog, _ ->
                dialog.dismiss()
            }
            alert.show()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        event_reycler.adapter = EventAdapter(EventModel(), listenerImpl)
    }
}