package com.example.sporty.event

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.sporty.R

class EventAdapter(
    private val model: EventModel,
    private val listener: Listener
) : RecyclerView.Adapter<EventAdapter.EventViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return EventViewHolder(inflater.inflate(R.layout.event_item, parent, false))
    }

    override fun getItemCount() = model.events.size

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        holder.setData(model.events[position])
    }

    inner class EventViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val sportType: TextView = itemView.findViewById(R.id.sport_type_value)
        val place: TextView = itemView.findViewById(R.id.place_value)
        val time: TextView = itemView.findViewById(R.id.time_value)
        val freeRecords: TextView = itemView.findViewById(R.id.records_value)
        val allRecords: TextView = itemView.findViewById(R.id.records_value_count)
        val additionButton: Button = itemView.findViewById(R.id.additional_info)
        val registerButton: Button = itemView.findViewById(R.id.register)

        init {
            additionButton.setOnClickListener {
                listener.onAdditionalInfoClicked(absoluteAdapterPosition)
            }

            registerButton.setOnClickListener {
                listener.onRegisterClicked(absoluteAdapterPosition)
            }
        }

        fun setData(event: Event) {
            sportType.text = event.sportName
            place.text = event.place
            time.text = event.date
            freeRecords.text = event.freePlace.toString()
            allRecords.text = event.allPlaceCount.toString()
        }
    }

    interface Listener {
        fun onAdditionalInfoClicked(index: Int)

        fun onRegisterClicked(index: Int)
    }
}