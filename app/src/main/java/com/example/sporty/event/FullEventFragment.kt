package com.example.sporty.event

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.example.sporty.R
import kotlinx.android.synthetic.main.fragment_full_info_fragment.*

class FullEventFragment: DialogFragment() {

    private lateinit var event: Event

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Translucent_NoTitleBar)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_full_info_fragment, container, false)
    }

    fun setEvent(event: Event) {
        this.event = event
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        name.text = "${event.name} ${event.surname}"
        sport_type_value.text = event.sportName
        place_value.text = event.place
        time_value.text = event.date
        cost_value.text = if (event.cost == 0) {
            "Бесплатно"
        } else {
            event.cost.toString()
        }

        level_value.text = event.level
        record_avaliable.text = event.freePlace.toString()
        record_all.text = event.allPlaceCount.toString()
        comment_value.text = event.Comment
        whatsapp_value.text = if (event.isWhatsAppEnabled) {
            "Есть"
        } else {
            "остуствует"
        }

        back.setOnClickListener {
            dismiss()
        }

        register.setOnClickListener {
            AppliedEvents.applyEvent(event)

            val alert = AlertDialog.Builder(requireContext())

            alert.setTitle("Вы записались на мероприятие")
            alert.setPositiveButton("Ок") { dialog, _ ->
                dialog.dismiss()
                dismiss()
            }

            alert.show()
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return object : Dialog(requireContext(), theme) {
            override fun onBackPressed() {
                dismiss()
            }
        }
    }
}