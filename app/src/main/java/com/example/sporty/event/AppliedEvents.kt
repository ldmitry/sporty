package com.example.sporty.event

object AppliedEvents {

    private val appliedEvents: MutableList<Event> = arrayListOf()

    private val createdByMeEvents: MutableList<Event> = arrayListOf(
        Event(
            "Лидия",
            "Поздеева",
            "Бадминтон",
            "Дворец спорта НГТУ, Блюхера 34",
            "25 июня 19:00",
            0,
            "Новичок",
            1,
            1,
            "Бадминтон возле Дворца спорта НГТУ",
            true
        )
    )

    private val visitedByMe: MutableList<Event> = arrayListOf(
        Event(
            "Лидия",
            "Поздеева",
            "Бадминтон",
            "Дворец спорта НГТУ, Блюхера 34",
            "25 июня 19:00",
            0,
            "Новичок",
            1,
            1,
            "Бадминтон возле Дворца спорта НГТУ",
            true
        )
    )


    fun applyEvent(event: Event) {
        appliedEvents.add(event)
    }

    fun saveCreatedEvent(event: Event) {
        createdByMeEvents.add(event)
    }

    fun getEvents() = appliedEvents.toList()

    fun getCreatedByMeEvents() = createdByMeEvents.toList()

    fun getVisitedEvents() = visitedByMe.toList()
}