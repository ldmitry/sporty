package com.example.sporty.event

data class Event(
    val name: String,
    val surname: String,
    val sportName: String,
    val place: String,
    val date: String,
    val cost: Int,
    val level: String,
    val freePlace: Int,
    val allPlaceCount: Int,
    val Comment: String,
    val isWhatsAppEnabled: Boolean
)