package com.example.sporty.eventcreation

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.sporty.R
import com.example.sporty.sportSelector.SportSelectorAdapter
import kotlinx.android.synthetic.main.fargment_event_creation.*

class EventCreationFragment: Fragment(R.layout.fargment_event_creation) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sport__name_recycler.adapter = SportSelectorAdapter()
        create_button.setOnClickListener {
            val alert = AlertDialog.Builder(requireContext())

            alert.setTitle("Мероприятие успешно создано")
            alert.setPositiveButton("Ок") { dialog, _ ->
                dialog.dismiss()
            }
            alert.show()
        }
    }
}