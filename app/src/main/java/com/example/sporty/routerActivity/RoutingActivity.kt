package com.example.sporty.routerActivity

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.sporty.MapsFragment
import com.example.sporty.R
import com.example.sporty.event.EventFragment
import com.example.sporty.eventcreation.EventCreationFragment
import com.example.sporty.filter.FilterFragment
import com.example.sporty.my_profile.MyProfileFragment
import com.example.sporty.myevents.MyEventFragment
import com.example.sporty.news.NewsFragment
import com.example.sporty.rate_event.RateDialog
import kotlinx.android.synthetic.main.bottom_bar.*
import kotlinx.android.synthetic.main.routing_activity.*

class RoutingActivity : AppCompatActivity(), IMapDelegate {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.routing_activity)

        events.setOnClickListener {
            openMap()
            selectEvents()
        }
        events_text.setOnClickListener {
            openMap()
            selectEvents()
        }

        heart.setOnClickListener {
            openNews()
            selectNews()
        }

        news_text.setOnClickListener {
            openNews()
            selectNews()
        }

        list_button.setOnClickListener {
            openEventList()
            map_button.backgroundTintList = ColorStateList.valueOf(Color.TRANSPARENT)
            map_button.setTextColor(ContextCompat.getColor(applicationContext, android.R.color.holo_blue_dark))

            list_button.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(applicationContext, android.R.color.holo_blue_dark))
            list_button.setTextColor(ContextCompat.getColor(applicationContext, android.R.color.white))
        }

        map_button.setOnClickListener {
            openMap()
        }

        filter.setOnClickListener {
            openFilter()
        }

        my_notes.setOnClickListener {
            openMyNotes()
            selectMyNotes()
        }

        my_notes_text.setOnClickListener {
            openMyNotes()
            selectMyNotes()
        }

        create_event.setOnClickListener {
            openEventCreation()
            selectCreateEvent()
        }

        profile.setOnClickListener {
            openProfile()
            selectProfile()
        }

        openMap()
        selectEvents()
    }

    private fun openMap() {
        switch_map_list_container.visibility = View.VISIBLE
        list_button.backgroundTintList = ColorStateList.valueOf(Color.TRANSPARENT)
        list_button.setTextColor(ContextCompat.getColor(applicationContext, android.R.color.holo_blue_dark))

        map_button.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(applicationContext, android.R.color.holo_blue_dark))
        map_button.setTextColor(ContextCompat.getColor(applicationContext, android.R.color.white))
        supportFragmentManager.beginTransaction().replace(R.id.placeHolder,
            MapsFragment()
        ).commitAllowingStateLoss()
    }

    private fun openNews() {
        switch_map_list_container.visibility = View.GONE

        supportFragmentManager.beginTransaction().replace(R.id.placeHolder,
            NewsFragment()
        ).commitAllowingStateLoss()
    }

    private fun openEventList() {
        switch_map_list_container.visibility = View.VISIBLE
        supportFragmentManager.beginTransaction().replace(R.id.placeHolder,
            EventFragment()
        ).commitAllowingStateLoss()
    }

    private fun openMyNotes() {
        switch_map_list_container.visibility = View.GONE
        supportFragmentManager.beginTransaction().replace(R.id.placeHolder,
            MyEventFragment()
        ).commitAllowingStateLoss()
    }

    private fun openProfile() {
        switch_map_list_container.visibility = View.GONE
        supportFragmentManager.beginTransaction().replace(R.id.placeHolder,
            MyProfileFragment()
        ).commitAllowingStateLoss()

        val fragment = RateDialog()
        fragment.show(supportFragmentManager, "BLABLA")
    }

    private fun openFilter() {
        val fragment = FilterFragment()

        fragment.show(supportFragmentManager, "FilterFragment")
    }

    private fun openEventCreation() {
        switch_map_list_container.visibility = View.GONE
        supportFragmentManager.beginTransaction().replace(R.id.placeHolder,
            EventCreationFragment()
        ).commitAllowingStateLoss()
    }

    override fun onMarkerClicked() {
        event_preview.visibility = View.VISIBLE
    }

    private fun selectNews() {
        setSelectedNews(true)
        setSelectedCreateEvents(false)
        setSelectedEvents(false)
        setSelectedMyNotes(false)
        setSelectedProfile(false)
    }

    private fun selectCreateEvent() {
        setSelectedNews(false)
        setSelectedCreateEvents(true)
        setSelectedEvents(false)
        setSelectedMyNotes(false)
        setSelectedProfile(false)
    }

    private fun selectEvents() {
        setSelectedNews(false)
        setSelectedCreateEvents(false)
        setSelectedEvents(true)
        setSelectedMyNotes(false)
        setSelectedProfile(false)
    }

    private fun selectMyNotes() {
        setSelectedNews(false)
        setSelectedCreateEvents(false)
        setSelectedEvents(false)
        setSelectedMyNotes(true)
        setSelectedProfile(false)
    }

    private fun selectProfile() {
        setSelectedNews(false)
        setSelectedCreateEvents(false)
        setSelectedEvents(false)
        setSelectedMyNotes(false)
        setSelectedProfile(true)
    }

    private fun setSelectedNews(isSelected: Boolean) {
        val color = if (isSelected) {
            R.color.pink
        } else {
            R.color.white
        }

        heart.setColorFilter(
            ContextCompat.getColor(applicationContext,
                color), android.graphics.PorterDuff.Mode.SRC_IN)

        news_text.setTextColor(ContextCompat.getColor(applicationContext, color))
    }

    private fun setSelectedMyNotes(isSelected: Boolean) {
        val color = if (isSelected) {
            R.color.pink
        } else {
            R.color.white
        }

        my_notes.setColorFilter(
            ContextCompat.getColor(applicationContext,
                color), android.graphics.PorterDuff.Mode.SRC_IN)

        my_notes_text.setTextColor(ContextCompat.getColor(applicationContext, color))
    }

    private fun setSelectedCreateEvents(isSelected: Boolean) {
        val color = if (isSelected) {
            R.color.pink
        } else {
            R.color.white
        }

        create_event.setColorFilter(
            ContextCompat.getColor(applicationContext,
                color), android.graphics.PorterDuff.Mode.SRC_IN)

        create_event_text.setTextColor(ContextCompat.getColor(applicationContext, color))
    }

    private fun setSelectedEvents(isSelected: Boolean) {
        val color = if (isSelected) {
            R.color.pink
        } else {
            R.color.white
        }

        events.setColorFilter(
            ContextCompat.getColor(applicationContext,
                color), android.graphics.PorterDuff.Mode.SRC_IN)

        events_text.setTextColor(ContextCompat.getColor(applicationContext, color))
    }

    private fun setSelectedProfile(isSelected: Boolean) {
        val color = if (isSelected) {
            R.color.pink
        } else {
            R.color.white
        }

        profile.setColorFilter(
            ContextCompat.getColor(applicationContext,
                color), android.graphics.PorterDuff.Mode.SRC_IN)

        profile_text.setTextColor(ContextCompat.getColor(applicationContext, color))
    }
}