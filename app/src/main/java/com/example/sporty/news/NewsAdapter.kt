package com.example.sporty.news

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sporty.R

class NewsAdapter : RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {

    val habits = arrayListOf<String>(
        "Больше пейте. Выпивайте каждое утро натощак стакан теплой воды с лимоном. Поставьте на рабочем столе бутылку с водой.",
        "Замените один гарнир в день на овощной салат. И добавьте в меню на каждый день хотя бы один фрукт.",
        "Начинайте утро с 10-15 минут физической активности. Найдите свой вариант: зарядка, йога, танцы, тренажер.",
        "Проверьте свою косметику на безопасность. Оставьте только качественную. И заодно убедитесь в актуальности сроков годности.",
        "Завтракайте в первые 30 минут после того, как проснулись.",
        "Физическая активность – позволяет поддерживать хороший тонус мышц и сердечно-сосудистой системы, служит хорошей профилактикой ожирения, нарушений работы двигательного аппарата, улучшает работу легких и ускоряет обмен веществ.",
        "Экология жилищных условий – уберегает от интоксикации некачественными материалами, поддерживает оптимальный уровень температуры и влажности."


    )

    val eats = arrayListOf<String>(
        ""
    )

    val trains = arrayListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return NewsViewHolder(inflater.inflate(R.layout.item_news, parent, false))
    }

    override fun getItemCount() = 3

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val items = when (position) {
            0 -> habits
            1 -> eats
            2 -> trains
            else -> emptyList<String>()
        }
        holder.setAdapter(items)
    }

    inner class NewsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        private val recyclerView = itemView.findViewById<RecyclerView>(R.id.item_news_recycler_view)

        fun setAdapter(list: List<String>) {
            recyclerView.adapter = NewsItemAdapter(list)
        }
    }
}

