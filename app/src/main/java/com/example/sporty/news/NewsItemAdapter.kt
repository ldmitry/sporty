package com.example.sporty.news

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.sporty.R

class NewsItemAdapter(private val news: List<String>) : RecyclerView.Adapter<NewsItemAdapter.ItemNewsViewHolder>() {

    inner class ItemNewsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        private val textView = itemView.findViewById<TextView>(R.id.news_info)

        fun setText(text: String) {
            textView.text = text
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemNewsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ItemNewsViewHolder(inflater.inflate(R.layout.item_one_news, parent, false))
    }

    override fun getItemCount() = news.size

    override fun onBindViewHolder(holder: ItemNewsViewHolder, position: Int) {
        holder.setText(news[position])
    }
}