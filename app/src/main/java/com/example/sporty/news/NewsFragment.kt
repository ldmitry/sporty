package com.example.sporty.news

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.sporty.R
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.fragment_news.*

class NewsFragment: Fragment(R.layout.fragment_news) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        news_view_pager.adapter = NewsAdapter()

        TabLayoutMediator(tabs, news_view_pager) { tab, position ->
            tab.text = when(position) {
                0 -> "Привычки"
                1-> "Питание"
                2 -> "Упражнения"
                else -> ""
            }
        }.attach()

    }
}